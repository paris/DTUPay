import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Customer")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account implements Serializable {

    private String name;
    private String accountID;

    public Account(String name, String accountID) {
        this.name = name;
        this.accountID = accountID;
    }

    public String getName() {
        return name;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAccountID(String AccountID) {
        this.accountID = accountID;
    }

    @Override
    public String toString() {
        return "Account{" +
                "name='" + name + '\'' +
                ", accountID='" + accountID + '\'' +
                '}';
    }
}
