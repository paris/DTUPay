import java.io.*;
import java.util.ArrayList;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "TokenService")
@XmlAccessorType(XmlAccessType.FIELD)
public class TokenService {
    // TODO: CREATE INTERFACE FOR THE SERVICE
    private GenBarcode gb = new GenBarcode();
    private Database db;

    {
        try {
            db = new Database();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /*
    Accepts a customer and an amount.
    If the amount is correct and the customer doesn't have too many unused tokens,
    creates the requested amount of tokens. Returns an ArrayList of the TokenID strings.
     */
    public ArrayList<Token> requestTokens(Customer customer, int amnt){
        ArrayList<Token> requestedTokens = new ArrayList<Token>();
        if (amnt > 5 || amnt < 1){ //!checkUnusedTokens(customer.getTokens())
            return null;
        }

        for (int i = 0; i < amnt; i++){
            Token t = createToken();
            requestedTokens.add(t);
            customer.getTokens().add(t);
        }

        // TODO: Check if customer already exist
        db.addCustomer(customer);

        return requestedTokens;
    }

    /*
    Creates a token, generates a barcode, adds the token to allTokens arraylist, returns the token.
     */
    private Token createToken() {
        try {
            UUID id = UUID.randomUUID();
            Token t = new Token(id, gb.generate(id.toString()), false);
            System.out.println(t);
            db.addToken(t);
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
//        String barcodeImagePath = null;
//        try {
//            barcodeImagePath = gb.generate(toString());
//            t.setBarcode(barcodeImagePath);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    /*
    Accepts a tokenID string. Finds and returns the token, if it exists. Else returns null.
     */
//    public Token lookupToken(Token tokenID) {
//        for (int i = 0; i < allTokens.size(); i++){
//            if (allTokens.get(i).getId() == tokenID){
//                return allTokens.get(i);
//            }
//        }
//        System.out.println("Token Rejected");
//        return null;
//    }

    /*
    Accepts a string, corresponding to a token ID.
    If a token can be looked up and is unused, returns true. Marks that token as used.
     */
//    public boolean useToken(UUID tokenID){
//        Token token = lookupToken(tokenID);
//        if (token == null || token.isUsed() ){
//            System.out.println("Token Rejected");
//            return false;
//        }
//        token.setUsed(true);
//        System.out.println("Token Accepted");
//        return true;
//    }

    /*
    Checks if the amount of unused tokens is 1 or less
     */
//    private boolean checkUnusedTokens(ArrayList<Token> custTokens){
//        int amntUnusedTokens = custTokens.size();
//        for (int i = 0; i < custTokens.size(); i++){
//            Token tmpToken = lookupToken(custTokens.get(i));
//            if (tmpToken.isUsed()){
//                amntUnusedTokens--;
//            }
//        }
//        if (amntUnusedTokens > 1){
//            return false;
//        }
//        return true;
//    }



}
