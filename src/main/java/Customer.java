import java.util.ArrayList;

public class Customer extends Account{

    private ArrayList<Token> tokens = new ArrayList<Token>();

    public Customer (String name, String accountID) {
        super(name, accountID);
    }

    public ArrayList<Token> getTokens() {
        return tokens;
    }

    public void setTokens(ArrayList<Token> tokens) {
        this.tokens = tokens;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", tokens='" + getTokens() +
                '}';
    }
}
