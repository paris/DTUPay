

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;





public class XmlConverter {
	private TokenService xmldtu ; // it needs it to store what it gives back
	private String xmladdress;  // where to read to and from
	
	// arg constructor
	public XmlConverter(String xmladdress , TokenService xmldtu) {
		this.xmladdress = xmladdress ;	
		this.xmldtu = xmldtu ;	
	}// end arg constructor

	// no arg constructor
	public XmlConverter() {}

	
	/// getters and setters
	public TokenService getXmldtu() {
		return xmldtu;
	}

	public void setXmldtu(TokenService xmldtu) {
		this.xmldtu = xmldtu;
	}

	public String getXmladdress() {
		return xmladdress;
	}

	public void setXmladdress(String xmladdress) {
		this.xmladdress = xmladdress;
	}
	////////////////////////////////////////////
	
	// it's the variable it gives back after it read it
	// it take
	public TokenService XmlAllReader(String xmladdress) throws JAXBException {
		// it has the type of the class
		JAXBContext jaxbContext = JAXBContext.newInstance(TokenService.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Source source = new StreamSource(xmladdress);

		JAXBElement<TokenService> root = jaxbUnmarshaller.unmarshal(source, TokenService.class);

		TokenService xmldtu = root.getValue();

		return xmldtu;
	}// end XmlReader

	// it neeeds an object of the actual system



	public void XmlAllWriter (String xmladdress , TokenService xmldtu) throws JAXBException { // it needs a TokenService to write to

		JAXBContext jaxbContext = JAXBContext.newInstance(TokenService.class);

		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();


		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


		jaxbMarshaller.marshal(xmldtu, new File(xmladdress));


		} //end XmlWriter		
			

}// end class


