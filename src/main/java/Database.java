import java.io.*;

public class Database {
    // TODO: Make database interface
    //public static ArrayList<Customer> allCustomers;
    private FileOutputStream customers, tokens;

    public Database () throws FileNotFoundException {
        this.customers = new FileOutputStream("customers");
        this.tokens = new FileOutputStream("tokens");
    }


    public void addCustomer(Account customer) {
        try {
            ObjectOutputStream objectOut = new ObjectOutputStream(customers);
            objectOut.writeObject(customer.toString());
            objectOut.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addToken (Token token) {
        try {
            ObjectOutputStream objectOut = new ObjectOutputStream(tokens);
            objectOut.writeObject(token.toString());
            objectOut.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean userExist(String customerID) {
        return true;
    }
}
