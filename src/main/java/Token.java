import java.io.Serializable;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "Token")
@XmlAccessorType(XmlAccessType.FIELD)
public class Token implements Serializable {
    private UUID id;
    private String barcode;
    private boolean used;

    public Token(UUID tokenID, String barcode, boolean used) {
        this.id = tokenID; //If phones cannot read this, change id generator method.
        this.barcode = barcode;
        this.used = used;
    }

    public UUID getId() {
        return id;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @Override
    public String toString() {
        return "Token{" +
                "id=" + id +
                ", barcode='" + barcode + '\'' +
                ", used=" + used +
                '}';
    }
}
